import speech_recognition as sr
from time import ctime
import time
import os
from gtts import gTTS
import requests, json

m = sr.Microphone()
r = sr.Recognizer()

i = 0
while i < 100000000 :
    with m as source:
        print ("say something")
        audio = r.listen(source)

    with open("microphone-results.wav", "wb") as f:
        f.write(audio.get_wav_data())

    from os import path
    AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)), "microphone-results.wav")

    print("I understood: " + r.recognize_google(audio))
    #Understands "hello"
    if r.recognize_google(audio) == "hello":
        print("Hey!")
    #Understands a basic question
    if r.recognize_google(audio) == "how are you":
        print("I am well")
    #Can say the current date and time
    if r.recognize_google(audio) == "what is today's date" or r.recognize_google(audio) == "what day is it today":
        print("I think the date and time is: " + ctime())
    #weather command
    if r.recognize_google(audio) == "what is the weather":
        api_key = "1b6a0865e80ade32496b866e198e4587"
        weather_url = "http://api.openweathermap.org/data/2.5/weather?"
        location = input("What city?")
        url = weather_url + "appid=" + api_key + "&q=" + location
        js = requests.get(url).json()
        if js["cod"] != "404":
            weather = js["main"]
            temp = weather["temp"]
            hum = weather["humidity"]
            desc = js["weather"][0]["description"]
            resp_string = " The temperature is " + str(temp) + " The humidity is " + str(hum) + " and The weather description is "+ str(desc)
            print(resp_string)
    #stop command
    if r.recognize_google(audio) == "stop" or r.recognize_google(audio) == "end":
        print ("Okay! Bye-bye, nice talk!")
        break

    time.sleep(2)
    i += 1
