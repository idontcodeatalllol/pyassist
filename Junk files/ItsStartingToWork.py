import speech_recognition as sr
from time import ctime
import time
import os
from gtts import gTTS
import requests, json

m = sr.Microphone()
r = sr.Recognizer()

with m as source:
    print ("say something")
    audio = r.listen(source)

with open("microphone-results.wav", "wb") as f:
    f.write(audio.get_wav_data())

from os import path
AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)), "microphone-results.wav")

print("I understood: " + r.recognize_google(audio))

if audio == "how are you":
    print("I am well")
    time.sleep(5)

with sr.AudioFile(AUDIO_FILE) as source:
    audio = r.record(source)
    #print (audio)
    time.sleep(5)

if "how are you" in AUDIO_FILE:
    print (audio)
    print("I am well")
    time.sleep(5)

if "what time is it" in AUDIO_FILE:
    print(ctime())
    time.sleep(5)
if "stop listening" in AUDIO_FILE:
    print("Listening stopped")
    time.sleep(5)

time.sleep(5)

#print (audio)
#def respond(audioString):
    #print(audio)
